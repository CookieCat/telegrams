(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
module.exports = function (css, customDocument) {
  var doc = customDocument || document;
  if (doc.createStyleSheet) {
    var sheet = doc.createStyleSheet()
    sheet.cssText = css;
    return sheet.ownerNode;
  } else {
    var head = doc.getElementsByTagName('head')[0],
        style = doc.createElement('style');

    style.type = 'text/css';

    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(doc.createTextNode(css));
    }

    head.appendChild(style);
    return style;
  }
};

module.exports.byUrl = function(url) {
  if (document.createStyleSheet) {
    return document.createStyleSheet(url).ownerNode;
  } else {
    var head = document.getElementsByTagName('head')[0],
        link = document.createElement('link');

    link.rel = 'stylesheet';
    link.href = url;

    head.appendChild(link);
    return link;
  }
};

},{}],2:[function(require,module,exports){
module.exports = require('cssify');
},{"cssify":1}],3:[function(require,module,exports){
module.exports = require('sassify')('.not-selectable {   user-select: none;   -moz-user-select: none; }  :root {   --transparent: rgba(255, 255, 255, 0);   --generic-font-color: #268dff;   --day-font-color: #333;   --day-meta-color: #8e8e8e;   --day-bck-color: #f9f9f9;   --day-hover-color: #abbacc;   --day-focus-color: #3d80d0;   --day-selected-color: #a2aab3;   --day-graph-meta-color: #ececec;   --day-graph-selection-color: #a2aab3;   --night-font-color: #dcdcdc;   --night-bck-color: #242f3e;   --night-meta-color: #405165;   --night-hover-color: #405165;   --night-focus-color: #aae1e2;   --night-selected-color: #163963;   --night-graph-meta-color: #1a2331;   --night-graph-selection-color: #163963; }  * {   font-family: "Open Sans", sans-serif !important;   font-size: 1rem;   box-sizing: border-box; } ');;
},{"sassify":2}],4:[function(require,module,exports){
module.exports = require('sassify')('@charset "UTF-8"; .page {   display: flex;   position: relative;   flex-flow: column nowrap;   width: 100vw;   height: 100vh;   padding: 1rem;   transition-property: background-color, color;   transition-duration: 300ms; }  .grid-graph-display {   margin-top: 1rem;   flex: 1; }  .grid-range-selector {   margin-top: 1rem;   height: 100px; }  .toggle-view {   margin-top: 1rem; }  .page[data-processing]::after {   content: \'подождите...\';   display: flex;   position: absolute;   top: 0;   left: 0;   width: 100vw;   height: 100vh;   place-content: center;   align-items: center;   color: var(--night-font-color);   background-color: rgba(0, 0, 0, 0.6); }  .page[data-theme=day] {   background-color: var(--day-bck-color);   color: var(--day-font-color); }  .page[data-theme=night] {   background-color: var(--night-bck-color);   color: var(--night-font-color); } ');;
},{"sassify":2}],5:[function(require,module,exports){
const BaseController = require('./primitive/BaseController');

const View = require('./App.View');
const Model = require('./App.Model');

const DataGrid = require('./primitive/DataGrid');

module.exports = class Controller extends BaseController {
  constructor(parent) {
    super('AppView.js', parent, Model, View);
  }

  /*
  ** Modify charts array
  */
  get charts() {
    return this.model.charts;
  }
  set charts(entries) {
    this.model.charts = Array.from(entries);
  }

  /*
  ** Set selected charts
  */
  set selectedChartsIndexes(indexes) {
    this.model.selectedChartsIndexes = Array.isArray(indexes)
      ? indexes.filter(Number.isSafeInteger)
      : Number.isSafeInteger(indexes)
        ? [indexes]
        : [];

    this.onSelectedChartsIndexesChanged(this.model.selectedChartsIndexes);
  }

  /*
  ** Sets theme
  */
  set theme(theme) {
    this.model.theme = theme;
  }

  /*
  ** Toggle next theme
  */
  nextTheme() {
    this.model.theme = this.model.nextTheme();
  }

  /*
  ** Occurs, when user selects new charts in dropdown
  */
  onSelectedChartsIndexesChanged(selectedIndexes) {
    this.model.isProcessing = true;

    this.model.slectionFrame = null;
    this.model.selectedChartsIndexes = Array.from(selectedIndexes);

    const grid = new DataGrid(this.model.extractSelectedCharts());

    this.model.grid = grid;
    this.model.selectionFrame = {
      left: 0.7,
      right: 0.9,
    };

    this.model.isProcessing = false;
  }

  /*
  ** User switched line visibility state
  */
  onLineToggled(event) {
    this.model.grid.toggleLine(event);
  }

  /*
  ** Selection frame changed on control
  */
  onSelectionFrameChanged(frame) {
    this.model.selectionFrame = Object.create(frame);
  }
}

},{"./App.Model":6,"./App.View":7,"./primitive/BaseController":38,"./primitive/DataGrid":44}],6:[function(require,module,exports){
const BaseModel = require('./primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      // app current theme
      theme: CONSTANTS.THEMES.NIGHT,
      // list of all avaliable charts to display
      charts: [],
      // list of selected chart's indexes
      selectedChartsIndexes: [],
      // selected graph's range
      selectionFrame: null,
      // grph's data grid
      grid: null,
      // defines busy app flag
      isProcessing: false,
    })
  }

  /*
  ** Get next theme
  */
  nextTheme() {
    return this.theme === CONSTANTS.THEMES.DAY
      ? CONSTANTS.THEMES.NIGHT
      : CONSTANTS.THEMES.DAY;
  }

  /*
  ** Extracts all currently selected (eg included in selectedChartsIndexes)
  ** charts and returns array
  */
  extractSelectedCharts() {
    return this.charts.filter((c, i) => this.selectedChartsIndexes.includes(i));
  }
}

},{"./primitive/BaseModel":39}],7:[function(require,module,exports){
require('../css/page.scss');

const BaseView = require('./primitive/BaseView');

const Button = require('./controls/button/button');
const grToggles = require('./controls/toggle_group/toggle_group');
const Selector = require('./controls/selector/selector');
const GridRangeSelector = require('./controls/grid_range_selector/range_selector');
const GridGraphDisplay = require('./controls/grid_graph_display/graph_display');

module.exports = class View extends BaseView {
    constructor(name, parent, model, controller) {
      super(name, parent, model, controller);
    }

    build(parent) {
      this.el = document.createElement('div');
      this.el.className = 'page';

      this.swSelectChart = new Selector('swSelectChart', this.el);
      this.grDisplay = new GridGraphDisplay('grDisplay', this.el);
      this.rsRangeSelector = new GridRangeSelector('rsRangeSelector', this.el);
      this.grToggles = new grToggles('grToggles', this.el);
      this.btnSwitchTheme = new Button('btnSwitchTheme', this.el);

      parent.appendChild(this.el);
    }

    exposeElements(controller) {
      controller.el = this.el;
      controller.swSelectChart = this.swSelectChart;
      controller.grDisplay = this.grDisplay;
      controller.rsRangeSelector = this.rsRangeSelector;
      controller.grToggles = this.grToggles;
      controller.btnSwitchTheme = this.btnSwitchTheme;
    }

    bindController(controller) {
      this.btnSwitchTheme.on('click', controller.nextTheme.bind(controller));
      this.grToggles.on('toggled', controller.onLineToggled.bind(controller));
      this.swSelectChart.on('selected', controller.onSelectedChartsIndexesChanged.bind(controller));
      this.rsRangeSelector.on('selectionFrameChanged', controller.onSelectionFrameChanged.bind(controller));
    }

    bindToModel(model) {
      model.onThemeChanged(this.onThemeChanged.bind(this));
      model.onGridChanged(this.onGridChanged.bind(this));
      model.onChartsChanged(this.onChartsChanged.bind(this));
      model.onSelectedChartsIndexesChanged(this.onSelectedChartsIndexesChanged.bind(this));
      model.onSelectionFrameChanged(this.onSelectionFrameChanged.bind(this));
    }

    /*
    ** MODEL EVENT HANLDERS
    */
    onThemeChanged(theme) {
      this.el.dataset['theme'] = theme;
      this.btnSwitchTheme.title = 'Switch to ' + this.model.nextTheme().capitalize() + ' Mode';
    }

    onChartsChanged(charts) {
      this.swSelectChart.items = charts.map(c => c.name);
      this.swSelectChart.selectedIndexes = charts.length ? [0] : [];
      this.grToggles.toggles = [];
      this.rsRangeSelector.selectionFrame = null;
    }

    onSelectedChartsIndexesChanged(indexes) {
      this.swSelectChart.selectedIndexes = indexes.slice();
      this.grToggles.toggles = this.model.charts
        .filter((c, i) => indexes.includes(i)) // get selected
        .map(c => c.lines) // return only lines
        .reduce((a, l) => { a.push(...l); return a; }, []) // compose all lines into single array
        .map(({uid, name, color, isEnabled}) => ({uid, title: name, color, isEnabled})); // return toggle models
    }

    onSelectionFrameChanged(range) {
      this.rsRangeSelector.selectionFrame = range;
      this.grDisplay.selectionFrame = range;
    }

    onGridChanged(grid) {
      this.rsRangeSelector.grid = grid;
      this.grDisplay.grid = grid;
    }
};

},{"../css/page.scss":4,"./controls/button/button":8,"./controls/grid_graph_display/graph_display":13,"./controls/grid_range_selector/range_selector":19,"./controls/selector/selector":23,"./controls/toggle_group/toggle_group":31,"./primitive/BaseView":40}],8:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./button.view');
const Model = require('./button.model');

module.exports = class Control extends BaseController {
  constructor(name, parent) {
    super(name, parent, Model, View);
  }

  set title(title) {
    this.model.title = title;
  }
}

},{"./../../primitive/BaseController":38,"./button.model":9,"./button.view":11}],9:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      title: '',
    });
  }
}

},{"./../../primitive/BaseModel":39}],10:[function(require,module,exports){
module.exports = require('sassify')('.flat-button {   width: 100%;   height: 2rem;   padding: 0;   margin: 1rem 0 0 0;   background-color: rgba(255, 255, 255, 0);   color: var(--generic-font-color);   border-style: none;   outline-style: none; } ');;
},{"sassify":2}],11:[function(require,module,exports){
require('./button.scss');

const BaseView = require('./../../primitive/BaseView');

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('button');
    this.el.className = 'flat-button';

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
  }

  bindController(controller) {
    this.el.addEventListener('click', controller.fire.bind(controller, 'click'));
  }

  bindToModel(model) {
    model.onTitleChanged(title => this.el.innerText = title);
  }

  setDefaults() {
    this.el.innerText = 'flat-button';
  }
}

},{"./../../primitive/BaseView":40,"./button.scss":10}],12:[function(require,module,exports){
module.exports = (superclass) => class extends superclass {
  /*
  ** wipe existing paths from screen
  */
  resetPaths() {
    this.paths.forEach(p => {
      p.style.opacity = 0;
      p.removeAttribute('d');
    });
  }

  /*
  ** Actually assign new D attribute to a path nodes
  */
  setPathsD() {
    const {meta} = this.model.grid;
    const {data, bounds} = this.model.extractedFrameData;
    const pathsUID = Object.keys(meta);

    // should draw paths only if we have something to draw - at least 2 points
    if (data.length < 2 || pathsUID.length === 0)
      return;

    const d = this.calculateDs(data, bounds, pathsUID);

    pathsUID.forEach((uid, i) => {
      const path = this.getPathByIndex(i)
      path.dataset['uid'] = uid;
      path.setAttribute('d', d[uid]);
      path.style.stroke = meta[uid].color;
      path.style.opacity = Number(meta[uid].isEnabled);
    });
  }

  calculateDs(data, {x_min, y_max}, pathsUID) {
    const scaleX = this.model.scaleX;
    const height = Number(this.svg.getAttribute('height'));
    const d = {};

    pathsUID.forEach(uid => {
      d[uid] = '';

      for (let i = 0; i < data.x.length; i++) {
        const x = data.x[i];
        const x_val = (x - x_min) / CONSTANTS.ONE_DAY_LENGTH * scaleX;

        if (Number.isNaN(x_val))
          continue;

        const y_val = (1 - data.y[i][uid] / y_max) * height;

        if (Number.isNaN(y_val))
          continue;

        if (d[uid].length)
          d[uid] += `L ${(x_val).toPrecision(3)} ${(y_val).toPrecision(3)} `;
        else
          d[uid] = `M ${(x_val).toPrecision(3)} ${(y_val).toPrecision(3)} `;
      }
    });

    return d;
  }

  /*
  ** Gets existing svg's path or creates new node
  */
  getPathByIndex(index) {
    let path = this.paths[index];

    if (path === void 0) {
      path = document.createElementNS(CONSTANTS.XMLNS, 'path');
      path.classList.add('path');

      this.svg.appendChild(path);
    }

    return path;
  }
};

},{}],13:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./graph_display.view');
const Model = require('./graph_display.model');

const DrawExtension = require('./graph_display.ext');

module.exports = class Control extends DrawExtension(BaseController) {
  constructor(name, parent) {
    super(name, parent, Model, View);

    /*
    ** throttle after window size changed
    */
    window.addEventListener(
      'resize',
      HELPERS.throttle(150, this, this.redraw)
    );
  }

  /*
  ** we use query... not getElement... since former is a live object
  */
  get paths() {
    return Array.from(this.svg && this.svg.querySelectorAll('.path') || []);
  }

  set grid(grid) {
    this.model.grid = grid;
  }

  set selectionFrame(range) {
    this.model.selectionFrame = range;
  }

  /*
  ** redraw controller completely
  */
  redraw() {
    if (this.model.grid === void 0 || this.model.grid.noDataAvaliable || this.model.selectionFrame === null)
      return this.el.classList.add('no-data');
    else
      this.el.classList.remove('no-data');

    this.svg.classList.add('hidden');

    this.updateSVGSize();
    this.calculateScale();
    this.updatePaths();

    this.svg.classList.remove('hidden');
  }

  /*
  ** updates SVG element size
  */
  updateSVGSize() {
    const {height, width} = this.el.getBoundingClientRect();

    this.svg.setAttribute('width', width);
    this.svg.setAttribute('height', height);
    this.svg.setAttribute('viewBox', `0 0 ${width} ${height}`);
    this.svg.setAttribute('xmlns', CONSTANTS.XMLNS);
    this.svg.style.width = width + 'px';
    this.svg.style.height = height + 'px';
  }

  calculateScale() {
    const {height, width} = this.el.getBoundingClientRect();

    this.model.extractedFrameData = this.model.grid.extractSelectionFrame(this.model.selectionFrame);
    this.model.scaleX = (width / (this.model.extractedFrameData.timelineLength - 1)).toPrecision(3);
  }

  updatePaths() {
    this.resetPaths();
    this.setPathsD();
  }
}

},{"./../../primitive/BaseController":38,"./graph_display.ext":12,"./graph_display.model":14,"./graph_display.view":16}],14:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      // grid
      grid: null,
      // range (null ==> all)
      selectionFrame: null,
      // scales
      scaleX: 1,
      // extracted frame data
      extractedFrameData: null,
    });
  }
}

},{"./../../primitive/BaseModel":39}],15:[function(require,module,exports){
module.exports = require('sassify')(':root {   --transparent: rgba(255, 255, 255, 0);   --generic-font-color: #268dff;   --day-font-color: #333;   --day-meta-color: #8e8e8e;   --day-bck-color: #f9f9f9;   --day-hover-color: #abbacc;   --day-focus-color: #3d80d0;   --day-selected-color: #a2aab3;   --day-graph-meta-color: #ececec;   --day-graph-selection-color: #a2aab3;   --night-font-color: #dcdcdc;   --night-bck-color: #242f3e;   --night-meta-color: #405165;   --night-hover-color: #405165;   --night-focus-color: #aae1e2;   --night-selected-color: #163963;   --night-graph-meta-color: #1a2331;   --night-graph-selection-color: #163963; }  .grid-graph-display {   position: relative;   overflow: hidden; }   .grid-graph-display svg {     display: inline-block;     fill: none;     transition: opacity 300ms;     opacity: 1; }   .grid-graph-display .path {     fill: none;     stroke-width: 0.1rem;     transform-origin: left bottom;     transition-property: opacity, d;     transition-duration: 300ms; }   .grid-graph-display .graph-cover {     position: absolute;     top: 0;     left: 0;     width: 100%;     height: 100%;     background-color: var(--transparent);     transition: opacity 300ms;     opacity: 1; }  .grid-graph-display.no-data svg {   opacity: 0; }  .grid-graph-display.no-data::after {   content: \'no data\';   display: flex;   place-content: center;   align-items: center;   position: absolute;   top: 0;   left: 0;   width: 100%;   height: 100%;   background-color: rgba(0, 0, 0, 0.2); }  *[data-theme=day] .grid-graph-display {   background-color: var(--day-graph-meta-color); }   *[data-theme=day] .grid-graph-display::after {     color: var(--meta-meta-color); }  *[data-theme=night] .grid-graph-display {   background-color: var(--night-graph-meta-color); }   *[data-theme=night] .grid-graph-display::after {     color: var(--night-meta-color); } ');;
},{"sassify":2}],16:[function(require,module,exports){
require('./graph_display.scss');

const BaseView = require('./../../primitive/BaseView');

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('div');
    this.el.className = 'grid-graph-display'

    this.svg = HELPERS.setAttr(document.createElementNS(CONSTANTS.XMLNS, 'svg'), {
      x: 0,
      y: 0,
    });

    this.cover = document.createElement('div');
    this.cover.className = 'graph-cover';

    this.el.appendChild(this.svg);
    this.el.appendChild(this.cover);

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
    controller.svg = this.svg;
  }

  bindController(controller) {
  }

  bindToModel(model) {
    model.onGridChanged(this.onGridChanged.bind(this));
    model.onSelectionFrameChanged(this.onSelectionFrameChanged.bind(this));
  }


  /*
  **
  ** MODEL EVENTS HANDLERS
  **
  */
  onGridChanged(grid) {
    this.controller.redraw(grid);

    // should listen to line's switch
    this.model.grid.on('lineVisibilityChanged', this.controller.redraw.bind(this.controller));
  }

  onSelectionFrameChanged(range) {
    if (range === null || this.model.grid === void 0 || this.model.grid.noDataAvaliable)
      return;

    this.controller.redraw();
  }
}

},{"./../../primitive/BaseView":40,"./graph_display.scss":15}],17:[function(require,module,exports){
module.exports = (superclass) => class extends superclass {
  /*
  ** wipe existing paths from screen
  */
  resetPaths() {
    this.paths.forEach(p => {
      p.style.opacity = 0;
      p.removeAttribute('d');
    });
  }

  /*
  ** Actually assign new D attribute to a path nodes
  */
  setPathsD() {
    const {meta, data, bounds} = this.model.grid;
    const pathsUID = Object.keys(meta);

    // should draw paths only if we have something to draw - at least 2 points
    if (data.length < 2 || pathsUID.length === 0)
      return;

    const d = this.calculateDs(data, bounds, pathsUID);

    pathsUID.forEach((uid, i) => {
      const path = this.getPathByIndex(i)
      path.dataset['uid'] = uid;
      path.setAttribute('d', d[uid]);
      path.style.stroke = meta[uid].color;
      path.style.opacity = Number(meta[uid].isEnabled);
    });
  }

  calculateDs(data, {x_min, y_max}, pathsUID) {
    const scaleX = this.model.scaleX;
    const height = Number(this.svg.getAttribute('height'));
    const d = {};

    pathsUID.forEach(uid => {
      d[uid] = '';

      for (let i = 0; i < data.x.length; i++) {
        const x = data.x[i];
        const x_val = (x - x_min) / CONSTANTS.ONE_DAY_LENGTH * scaleX;

        if (Number.isNaN(x_val))
          continue;

        const y_val = (1 - data.y[i][uid] / y_max) * height;

        if (Number.isNaN(y_val))
          continue;

        if (d[uid].length)
          d[uid] += `L ${(x_val).toPrecision(3)} ${(y_val).toPrecision(3)} `;
        else
          d[uid] = `M ${(x_val).toPrecision(3)} ${(y_val).toPrecision(3)} `;
      }
    });

    return d;
  }

  /*
  ** Gets existing svg's path or creates new node
  */
  getPathByIndex(index) {
    let path = this.paths[index];

    if (path === void 0) {
      path = document.createElementNS(CONSTANTS.XMLNS, 'path');
      path.classList.add('path');

      this.svg.appendChild(path);
    }

    return path;
  }
};

},{}],18:[function(require,module,exports){
/*
** Defines state of range selector when dragging
*/
const RANGE_STATE = {
  LEFT: 0,
  MIDDLE: 1,
  RIGHT: 2,
};

module.exports = (superclass) => class extends superclass {
  enableInput() {
    this.cover.onblur = this.onBlur.bind(this);
    this.cover.onmouseleave = this.onBlur.bind(this);

    // for mobile
    if ('ontouchstart' in document.documentElement) {
      this.cover.ontouchstart = HELPERS.debounce(30, this, this.onTouchStart);
      this.cover.ontouchmove = HELPERS.debounce(30, this, this.onTouchMove);
      this.cover.ontouchend = HELPERS.debounce(30, this, this.onTouchEnd);
    } else {
      this.cover.onmousedown = HELPERS.debounce(30, this, this.onMouseDown);
      this.cover.onmousemove = HELPERS.debounce(30, this, this.onMouseMove);
      this.cover.onmouseup = HELPERS.debounce(30, this, this.onMouseUp);
    }
  }

  onBlur() {
    this.pointerLost();
  }

  // desktop
  onMouseDown(event) {
    const offsetX = this.extractOffsetX_dsk(event);
    this.onStartAction(offsetX);
  }

  onMouseMove(event) {
    const offsetX = this.extractOffsetX_dsk(event);
    this.onMoveAction(offsetX);
  }

  onMouseUp() {
    this.onEndAction();
  }

  // mobile
  onTouchStart({touches}) {
    const offsetX = this.extractOffsetX_mob(touches);
    this.onStartAction(offsetX);
  }

  onTouchMove({touches}) {
    const offsetX = this.extractOffsetX_mob(touches);
    this.onMoveAction(offsetX);
  }

  onTouchEnd() {
    this.onEndAction();
  }


  /*
  ** Platform-agnostic implementation
  */
  onStartAction(offsetX) {
    const range = this.model.selectionFrame;
    const width = this.svg.getAttribute('width');
    const hotpoint = offsetX / width;

    this.isDragging = true;
    this.originalOffset = offsetX;
    this.originalRange = Object.create(this.model.selectionFrame);
    this.state = hotpoint < range.left
      ? RANGE_STATE.LEFT
      : hotpoint > range.right
        ? RANGE_STATE.RIGHT
        : RANGE_STATE.MIDDLE;
  }

  onMoveAction(offsetX) {
    if (this.isDragging === false || this.originalOffset === void 0 || this.state === void 0)
      return;

    const width = this.svg.getAttribute('width');
    const delta = (offsetX - this.originalOffset) / width;

    const alterLeft = Math.clamp(0, 1, this.originalRange.left + delta);
    const alterRight = Math.clamp(0, 1, this.originalRange.right + delta);

    this.moveRange(alterLeft, alterRight);
  }

  onEndAction() {
    this.pointerLost();
    this.fire('selectionFrameChanged', this.model.selectionFrame);
  }


  /*
  ** Helpers
  */
  pointerLost() {
    this.isDragging = false;
    this.originalOffset = void 0;
    this.originalRange = void 0;
    this.state = void 0;
  }

  extractOffsetX_dsk(event) {
    return this._extractOffset(event);
  }

  extractOffsetX_mob(touches) {
    if (touches.length === 0 || touches.item(0) === void 0)
      return 0;

    return this._extractOffset(touches.item(0));
  }

  _extractOffset({target, clientX}) {
    const {left, width} = target.parentElement.getBoundingClientRect();

    return Math.clamp(0, width, clientX - left);
  }

  moveRange(alterLeft, alterRight) {
    if (this.state === RANGE_STATE.LEFT) {
      if (alterLeft < this.originalRange.right)
        this.model.selectionFrame = {
          left: alterLeft,
          right: this.originalRange.right,
        };
    }

    if (this.state === RANGE_STATE.MIDDLE) {
      if (alterLeft < alterRight)
        this.model.selectionFrame = {
          left: alterLeft,
          right: alterRight,
        };
    }

    if (this.state === RANGE_STATE.RIGHT) {
      if (alterRight > this.originalRange.left)
        this.model.selectionFrame = {
          left: this.originalRange.left,
          right: alterRight,
        };
    }
  }
}

},{}],19:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./range_selector.view');
const Model = require('./range_selector.model');

const SelectorInputExtension = require('./range_selector.input');
const SelectorExtension = require('./range_selector.ext');

module.exports = class Control extends SelectorInputExtension(SelectorExtension(BaseController)) {
  constructor(name, parent) {
    super(name, parent, Model, View);

    /*
    ** Enable user input extension if is ok
    */
    this.enableInput && this.enableInput();

    /*
    ** throttle after window size changed
    */
    window.addEventListener(
      'resize',
      HELPERS.throttle(150, this, this.redraw)
    );
  }

  /*
  ** we use query... not getElement... since former is a live object
  */
  get paths() {
    return Array.from(this.svg && this.svg.querySelectorAll('.path') || []);
  }

  set grid(grid) {
    this.model.grid = grid;
  }

  set selectionFrame(range) {
    this.model.selectionFrame = range;
  }

  /*
  ** redraw controller completely
  */
  redraw() {
    if (this.model.grid === void 0 || this.model.grid.noDataAvaliable)
      return this.el.classList.add('no-data');
    else
      this.el.classList.remove('no-data');

    this.svg.classList.add('hidden');

    this.updateSVGSize();
    this.calculateScale();
    this.updatePaths();

    this.svg.classList.remove('hidden');
  }

  /*
  **
  ** FUNCTIONALITY
  **
  */

  /*
  ** updates SVG element size
  */
  updateSVGSize() {
    const size = this.el.getBoundingClientRect();

    this.svg.setAttribute('width', size.width);
    this.svg.setAttribute('height', size.height);
    this.svg.setAttribute('viewBox', `0 0 ${size.width} ${size.height}`);
    this.svg.setAttribute('xmlns', CONSTANTS.XMLNS);
    this.svg.style.width = size.width + 'px';
    this.svg.style.height = size.height + 'px';
  }

  calculateScale() {
    this.model.scaleX = (this.svg.getAttribute('width') / (this.model.grid.timelineLength - 1)).toPrecision(3);
  }

  updatePaths() {
    this.resetPaths();
    this.setPathsD();
  }
}

},{"./../../primitive/BaseController":38,"./range_selector.ext":17,"./range_selector.input":18,"./range_selector.model":20,"./range_selector.view":22}],20:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      // grid
      grid: null,
      // range (null ==> all)
      selectionFrame: null,
      // scale on X (we don't need Y scale, since it is alerady scaled for us!)
      scaleX: 1,
    });
  }
}

},{"./../../primitive/BaseModel":39}],21:[function(require,module,exports){
module.exports = require('sassify')(':root {   --transparent: rgba(255, 255, 255, 0);   --generic-font-color: #268dff;   --day-font-color: #333;   --day-meta-color: #8e8e8e;   --day-bck-color: #f9f9f9;   --day-hover-color: #abbacc;   --day-focus-color: #3d80d0;   --day-selected-color: #a2aab3;   --day-graph-meta-color: #ececec;   --day-graph-selection-color: #a2aab3;   --night-font-color: #dcdcdc;   --night-bck-color: #242f3e;   --night-meta-color: #405165;   --night-hover-color: #405165;   --night-focus-color: #aae1e2;   --night-selected-color: #163963;   --night-graph-meta-color: #1a2331;   --night-graph-selection-color: #163963; }  .grid-range-selector {   position: relative;   overflow: hidden; }   .grid-range-selector svg {     display: inline-block;     fill: none;     transition: opacity 300ms;     opacity: 1; }   .grid-range-selector .path {     fill: none;     stroke-width: 0.1rem;     transform-origin: left bottom;     transition-property: opacity, d;     transition-duration: 300ms; }   .grid-range-selector .margin {     position: absolute;     top: 0;     bottom: 0;     min-width: 0.5rem;     border-style: solid;     border-width: 0;     transition: opacity 300ms;     opacity: 0.8; }     .grid-range-selector .margin.left {       left: 0;       border-right-width: 0.5rem; }     .grid-range-selector .margin.right {       right: 0;       border-left-width: 0.5rem; }   .grid-range-selector .margin-cover {     position: absolute;     top: 0;     left: 0;     width: 100%;     height: 100%;     background-color: var(--transparent);     transition: opacity 300ms;     opacity: 1; }  .grid-range-selector.no-data svg, .grid-range-selector.no-data .margin, .grid-range-selector.no-data .margin-cover {   opacity: 0; }  .grid-range-selector.no-data::after {   content: \'no data\';   display: flex;   place-content: center;   align-items: center;   position: absolute;   top: 0;   left: 0;   width: 100%;   height: 100%;   background-color: rgba(0, 0, 0, 0.2); }  *[data-theme=day] .grid-range-selector {   background-color: var(--day-graph-meta-color); }   *[data-theme=day] .grid-range-selector::after {     color: var(--meta-meta-color); }   *[data-theme=day] .grid-range-selector .margin {     color: var(--meta-meta-color);     background-color: var(--day-selected-color); }  *[data-theme=night] .grid-range-selector {   background-color: var(--night-graph-meta-color); }   *[data-theme=night] .grid-range-selector::after {     color: var(--night-meta-color); }   *[data-theme=night] .grid-range-selector .margin {     color: var(--night-meta-color);     background-color: var(--night-selected-color); } ');;
},{"sassify":2}],22:[function(require,module,exports){
require('./range_selector.scss');

const BaseView = require('./../../primitive/BaseView');

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('div');
    this.el.className = 'grid-range-selector'

    this.svg = HELPERS.setAttr(document.createElementNS(CONSTANTS.XMLNS, 'svg'), {
      x: 0,
      y: 0,
    });

    this.leftMargin = document.createElement('div');
    this.leftMargin.className = 'margin left';

    this.rightMargin = document.createElement('div');
    this.rightMargin.className = 'margin right';

    this.cover = document.createElement('div');
    this.cover.className = 'margin-cover';

    this.el.appendChild(this.svg);
    this.el.appendChild(this.leftMargin);
    this.el.appendChild(this.rightMargin);
    this.el.appendChild(this.cover);

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
    controller.svg = this.svg;
    controller.leftMargin = this.leftMargin;
    controller.rightMargin = this.rightMargin;
    controller.cover = this.cover;
  }

  bindController(controller) {
  }

  bindToModel(model) {
    model.onGridChanged(this.onGridChanged.bind(this));
    model.onSelectionFrameChanged(this.onSelectionFrameChanged.bind(this));
  }


  /*
  **
  ** MODEL EVENTS HANDLERS
  **
  */
  onGridChanged(grid) {
    this.controller.redraw(grid);

    // should listen to line's switch
    this.model.grid.on('lineVisibilityChanged', this.controller.redraw.bind(this.controller));
  }

  onSelectionFrameChanged(range) {
    if (range === null || this.model.grid === void 0 || this.model.grid.noDataAvaliable)
      return;

    const width = this.svg.getAttribute('width');
    const rOffset = width * range.right;
    const rWidth = width - rOffset;

    this.leftMargin.style.width = width * range.left + 'px';
    this.rightMargin.style.width = rWidth + 'px';
  }
}

},{"./../../primitive/BaseView":40,"./range_selector.scss":21}],23:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./selector.view');
const Model = require('./selector.model');

module.exports = class Control extends BaseController {
  constructor(name, parent) {
    super(name, parent, Model, View);
  }

  /*
  ** Toggle items list
  */
  get isExpanded() {
    return this.model.isExpanded;
  }
  set isExpanded(state) {
    this.model.isExpanded = state;
    this.fire('toggled', this.model);
  }

  /*
  ** manage items list
  */
  get items() {
    return (this.model.items || []).slice();
  }
  set items(items) {
    this.model.batch({
      items: Array.from(items),
      selectedIndexes: [],
      selectionText: '',
    })

    this.previousIndexes = null;
  }

  /*
  ** Change selection indexes
  */
  get selectedIndexes() {
    return (this.model.selectedIndexes || []).slice();
  }
  set selectedIndexes(indexes) {
    this.model.selectedIndexes = Array.isArray(indexes)
      ? indexes.filter(Number.isSafeInteger)
      : Number.isSafeInteger(indexes)
        ? [indexes]
        : [];
    this.model.selectedIndexes.sort();

    this.model.selectionText = this.selectedIndexes.map(i => this.items[i]).join('; ');
  }

  /*
  **
  */
  onOpenList(event) {
    event.stopPropagation();
    event.cancelBubble = true;

    this.isExpanded = this.isExpanded || true;
  }

  /*
  ** Action on items clicked
  */
  onItemClicked(event) {
    const {target: {index}} = event;

    if (index === void 0 || index < 0)
      return;

    event.stopPropagation();
    event.cancelBubble = true;

    const set = new Set(this.selectedIndexes);
    const wasSelected = set.has(index);

    set[
      wasSelected ? 'delete' : 'add'
    ](index);

    this.selectedIndexes = Array.from(set);
  }

  /*
  ** Execute this when list is closed via 'done' button
  */
  onSelectionDone(event) {
    if (this.isExpanded === false)
      return;

    event.stopPropagation();
    event.cancelBubble = true;

    this.isExpanded = false;

    if (HELPERS.isNotSameArray(this.previousIndexes, this.selectedIndexes))
      this.fire('selected', this.selectedIndexes);

    this.previousIndexes = this.selectedIndexes.slice();
  }

  /*
  ** Close list on click outside
  */
  onClickOutside() {
    if (this.isExpanded === false)
      return;

    this.isExpanded = false;
    this.selectedIndexes = this.previousIndexes || [];
  }
}

},{"./../../primitive/BaseController":38,"./selector.model":24,"./selector.view":26}],24:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      isExpanded: false,
      items: [],
      selectionText: '',
      selectionPlaceholder: 'nothing is selected...',
      selectedIndexes: [],
    });
  }
}

},{"./../../primitive/BaseModel":39}],25:[function(require,module,exports){
module.exports = require('sassify')('.not-selectable, .selector {   user-select: none;   -moz-user-select: none; }  .selector {   position: relative;   padding: 0;   border-width: 1px;   border-style: solid;   border-radius: 5px;   line-height: 1.2rem; }  .selector * {   transition: opacity 300ms, transform 500ms; }  .selector .title {   padding: 1rem 0.6rem;   height: 3rem;   background-color: var(--transparent);   border-style: none;   outline-style: none;   width: 100%;   user-select: none;   pointer-events: none; }  .selector .items-holder {   position: absolute;   overflow: hidden;   top: 100%;   left: 1%;   width: 98%;   padding: 1rem 0.6rem;   margin: -2.7rem 0 0;   border-width: 1px;   border-style: solid;   border-radius: 5px;   transform-origin: top center;   transform: scaleY(0);   opacity: 0;   z-index: 10; }   .selector .items-holder .items {     list-style: none;     padding: 0 5rem 0 0;     margin: 0 -5rem 0 0;     max-height: 10rem;     overflow-x: hidden;     overflow-y: scroll; }     .selector .items-holder .items .item {       padding: 0.3rem 0.6rem 0.2rem;       border-radius: 1rem;       margin-bottom: 0.3rem; }   .selector .items-holder .close-button {     width: 100%;     padding: 0.4rem 0.2rem;     outline-style: none;     border-style: solid;     border-radius: 1rem;     font-size: 0.8rem; }  .selector[data-expanded] .title::before {   opacity: 0; }  .selector[data-expanded] .items-holder {   transform: scaleY(1);   opacity: 1; }  /* themes : day */ *[data-theme=day] .selector {   border-color: var(--day-meta-color); }   *[data-theme=day] .selector .title,   *[data-theme=day] .selector .title::placeholder {     color: var(--day-font-color); }   *[data-theme=day] .selector .items-holder {     border-color: var(--day-meta-color);     background-color: var(--day-bck-color);     box-shadow: 0px 2px 8px var(--day-meta-color); }   *[data-theme=day] .selector .item:active {     background-color: var(--day-hover-color); }   *[data-theme=day] .selector .item:not(:active).selected {     background-color: var(--day-selected-color); }   *[data-theme=day] .selector .close-button {     border-color: var(--day-meta-color);     background-color: var(--day-bck-color);     color: var(--day-font-color); }   *[data-theme=day] .selector .close-button:active {     border-color: var(--day-focus-color); }  /* themes : night */ *[data-theme=night] .selector {   border-color: var(--night-meta-color); }   *[data-theme=night] .selector .title,   *[data-theme=night] .selector .title::placeholder {     color: var(--night-font-color); }   *[data-theme=night] .selector .items-holder {     border-color: var(--night-meta-color);     background-color: var(--night-bck-color);     box-shadow: 0px 2px 8px var(--night-meta-color); }   *[data-theme=night] .selector .item:active {     background-color: var(--night-hover-color); }   *[data-theme=night] .selector .item:not(:active).selected {     background-color: var(--night-selected-color); }   *[data-theme=night] .selector .close-button {     border-color: var(--night-meta-color);     background-color: var(--night-bck-color);     color: var(--night-font-color); }   *[data-theme=night] .selector .close-button:active {     border-color: var(--night-focus-color); } ');;
},{"sassify":2}],26:[function(require,module,exports){
require('./selector.scss');

const BaseView = require('./../../primitive/BaseView');

const EXPANDED = 'expanded';

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('div');
    this.el.className = 'selector';

    this.text = document.createElement('input');
    this.text.className = 'title';

    this.itemsListHolder = document.createElement('div');
    this.itemsListHolder.className = 'items-holder';

    this.itemsList = document.createElement('ul');
    this.itemsList.className = 'items';

    this.btnClose = document.createElement('button');
    this.btnClose.className = 'close-button';
    this.btnClose.innerText = 'Done';

    this.el.appendChild(this.text);
    this.el.appendChild(this.itemsListHolder);
    this.itemsListHolder.appendChild(this.itemsList);
    this.itemsListHolder.appendChild(this.btnClose);

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
    controller.text = this.text;
    controller.btnClose = this.btnClose;
    controller.itemsList = this.itemsList;
  }

  bindController(controller) {
    this.el.addEventListener('click', controller.onOpenList.bind(controller));
    this.btnClose.addEventListener('click', controller.onSelectionDone.bind(controller));
    // bind global event
    document.body.addEventListener('click', controller.onClickOutside.bind(controller));
  }

  bindToModel(model) {
    model.onIsExpandedChanged(this.onIsExpandedChanged.bind(this));
    model.onItemsChanged(this.onItemsChanged.bind(this));
    model.onSelectionTextChanged(this.onSelectionTextChanged.bind(this));
    model.onSelectionPlaceholderChanged(this.onSelectionPlaceholderChanged.bind(this));
    model.onSelectedIndexesChanged(this.onSelectedIndexesChanged.bind(this));
  }

  setDefaults() {
    this.text.setAttribute('placeholder', 'nothing is selected...');
  }

  /*
  ** MODEL EVENT HANLDERS
  */
  onIsExpandedChanged(state) {
    state
      ? this.el.dataset[EXPANDED] = 'true'
      : delete this.el.dataset[EXPANDED];
  }

  onItemsChanged(items) {
    const children = Array.from(this.itemsList.children);

    children.forEach(el => el.style.display = 'none');

    Array.from(items).forEach((item, index) => {
      let el = null;

      if (children[index] !== void 0)
        el = children[index];
      else
        el = document.createElement('li');

      el.className = "item";
      el.dataset['index'] = index;
      el.style.disaply = 'block';
      el.index = index;
      el.innerText = item;
      el.onclick = this.controller.onItemClicked.bind(this.controller);

      this.itemsList.appendChild(el);
    });
  }

  onSelectionTextChanged(text) {
    this.text.value = text;
  }

  onSelectionPlaceholderChanged(placeholder) {
    this.text.setAttribute('placeholder', placeholder);
  }

  onSelectedIndexesChanged(indexies) {
    const children = Array.from(this.itemsList.children);

    children.forEach(el => {
      const isSelected = indexies.includes(el.index);

      el.classList[
        isSelected ? 'add' : 'remove'
      ]('selected');
    });
  }
}

},{"./../../primitive/BaseView":40,"./selector.scss":25}],27:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./toggle.view');
const Model = require('./toggle.model');

module.exports = class Control extends BaseController {
  constructor(name, parent) {
    super(name, parent, Model, View);
  }

  /*
  ** Get/Set toggle state
  */
  get isEnabled() {
    return this.model.isEnabled;
  }
  set isEnabled(state) {
    this.model.isEnabled = state;
  }

  /*
  ** set toggle title
  */
  set title(title) {
    this.model.title = title;
  }

  /*
  ** set nib color
  */
  set color(color) {
    this.model.color = color;
  }

  /*
  ** Toggle controls's state
  */
  onToggle() {
    this.isEnabled = !this.isEnabled;
    this.fire('toggled', {uid: this.model.uid, state: this.isEnabled});
  }
}

},{"./../../primitive/BaseController":38,"./toggle.model":28,"./toggle.view":30}],28:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      uid: null,
      isEnabled: false,
      title: 'none',
      color: '#fff',
    });
  }
}

},{"./../../primitive/BaseModel":39}],29:[function(require,module,exports){
module.exports = require('sassify')('.not-selectable, .toggle-button {   user-select: none;   -moz-user-select: none; }  .toggle-button {   margin: 0.2rem 0.4rem;   background-color: rgba(255, 255, 255, 0);   outline-style: none;   border-style: solid;   border-width: 1px;   border-radius: 2rem;   overflow: hidden; }   .toggle-button .icon {     display: inline-block;     margin: 0.4rem 0.4rem 0.4rem 0.1rem;     width: 1.4rem;     height: 1.4rem;     line-height: 1rem;     vertical-align: middle; }     .toggle-button .icon .circle-inner {       transition: transform 300ms;       transform-origin: center;       transform: scale(0); }     .toggle-button .icon .line {       fill: rgba(255, 255, 255, 0);       stroke-width: 2px;       stroke: #efefef; }   .toggle-button .title {     display: inline-block;     margin: 0.5rem;     height: 1rem;     line-height: 1rem;     font-weight: 100;     vertical-align: middle; }  .toggle-button:not([data-toggled]) .circle-inner {   transform: scale(1); }  .toggle-button:not([data-toggled]) .line {   stroke: rgba(255, 255, 255, 0); }  /* themes : day */ *[data-theme=day] .toggle-button {   color: var(--day-font-color);   border-color: var(--day-meta-color); }   *[data-theme=day] .toggle-button:active {     border-color: var(--day-focus-color); }   *[data-theme=day] .toggle-button .title {     color: var(--day-font-color); }   *[data-theme=day] .toggle-button .circle-inner {     fill: var(--day-bck-color); }  /* themes : night */ *[data-theme=night] .toggle-button {   color: var(--night-font-color);   border-color: var(--night-meta-color); }   *[data-theme=night] .toggle-button:active {     border-color: var(--night-focus-color); }   *[data-theme=night] .toggle-button .title {     color: var(--night-font-color); }   *[data-theme=night] .toggle-button .circle-inner {     fill: var(--night-bck-color); } ');;
},{"sassify":2}],30:[function(require,module,exports){
require('./toggle.scss');

const BaseView = require('./../../primitive/BaseView');

const CHECKER = `
<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px">
  <g stroke-linecap="round">
    <circle class="circle-outer" fill="currentColor" cx="11px" cy="11px" r="10px" />
    <circle class="circle-inner" cx="11px" cy="11px" r="8px" />
    <path class="line" d="M 7,12 L 10,15 L 16,8" />
  </g>
</svg>
`;

const STATE = 'toggled';

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('button');
    this.el.className = "toggle-button";

    this.icon = document.createElement('span');
    this.icon.className = 'icon';
    this.icon.innerHTML = CHECKER;

    this.title = document.createElement('span');
    this.title.className = 'title';

    this.el.appendChild(this.icon);
    this.el.appendChild(this.title);

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
  }

  bindController(controller) {
    this.el.addEventListener('click', controller.onToggle.bind(controller));
  }

  bindToModel(model) {
    model.onIsEnabledChanged(this.onIsEnabledChanged.bind(this));
    model.onTitleChanged(this.onTitleChanged.bind(this));
    model.onColorChanged(this.onColorChanged.bind(this));
  }

  setDefaults() {
    this.title.innerText = 'none';
  }

  /*
  ** MODEL EVENT HANLDERS
  */
  onIsEnabledChanged(state) {
    state
      ? this.el.dataset[STATE] = 'true'
      : delete this.el.dataset[STATE];
  }

  onTitleChanged(title) {
    this.title.innerText = title;
  }

  onColorChanged(color = '#fff') {
    this.icon.style['color'] = color;
  }
}

},{"./../../primitive/BaseView":40,"./toggle.scss":29}],31:[function(require,module,exports){
const BaseController = require('./../../primitive/BaseController');

const View = require('./toggle_group.view');
const Model = require('./toggle_group.model');

module.exports = class Control extends BaseController {
  constructor(name, parent) {
    super(name, parent, Model, View);
  }

  /*
  ** Sets toggles, expects as array of [ToggleModel]
  */
  set toggles(models) {
    this.model.toggles = Array.from(models);
  }

  onToggleInvoked(e) {
    this.fire('toggled', e);
  }
}

},{"./../../primitive/BaseController":38,"./toggle_group.model":32,"./toggle_group.view":33}],32:[function(require,module,exports){
const BaseModel = require('./../../primitive/BaseModel');

module.exports = class Model extends BaseModel {
  constructor() {
    super({
      toggles: []
    });
  }
}

},{"./../../primitive/BaseModel":39}],33:[function(require,module,exports){
const BaseView = require('./../../primitive/BaseView');
const Toggle = require('./../toggle/toggle');

const EXPANDED = 'expanded';

module.exports = class View extends BaseView {
  constructor(name, parent, model, controller) {
    super(name, parent, model, controller);
  }

  build(parent) {
    this.el = document.createElement('div');
    this.el.className = 'toggle-view';

    parent.appendChild(this.el);
  }

  exposeElements(controller) {
    controller.el = this.el;
  }

  bindToModel(model) {
    model.onTogglesChanged(this.onTogglesChanged.bind(this));
  }

  /*
  ** MODEL EVENT HANLDERS
  */
  onTogglesChanged(models) {
    this.el.innerHTML = '';

    Array.from(models).forEach((model, index) => {
      const toggle = new Toggle(`tgLineToggle${index}`, this.el);

      toggle.model.batch(model);
      toggle.on('toggled', this.controller.onToggleInvoked.bind(this.controller));
    });
  }
}

},{"./../../primitive/BaseView":40,"./../toggle/toggle":27}],34:[function(require,module,exports){
module.exports = window.CONSTANTS = Object.freeze({
  XMLNS: 'http://www.w3.org/2000/svg',
  THEMES: Object.freeze({
    DAY: 'day',
    NIGHT: 'night',
  }),
  DATA_URI: '/assets/data/data.json',
  PART_TYPE: {
    X_AXIS: 'x',
    LINE: 'line',
  },
  ONE_DAY_LENGTH: 24 * 60 * 60 * 1000,
  TWO_DAYS_LENGTH: 2 * 24 * 60 * 60 * 1000,
});

},{}],35:[function(require,module,exports){
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1)
};

Math.clamp = function(min, max, value) {
  return value > max ? max : value < min ? min : value;
};

},{}],36:[function(require,module,exports){
function fetch(uri) {
  const req = new XMLHttpRequest();

  return new Promise((res, rej) => {
    req.onload = res;
    req.onerror = rej;
    req.open("GET", uri);
    req.send();
  });
};

function throttle(cooldown, ctx, callback) {
  let timerId = null;

  return (arguments) => {
    clearTimeout(timerId);

    timerId = setTimeout(() => {
      timerId = null;
      try {
        callback.call(ctx, arguments);
      } catch (error) {
        console.warn('Error on throttled function call');
        console.warn(error);
      }
    }, cooldown);
  }
};

function debounce(cooldown, ctx, callback) {
  return (arguments) => {
    setTimeout(() => {
      try {
        callback.call(ctx, arguments);
      } catch (error) {
        console.warn('Error on throttled function call');
        console.warn(error);
      }
    }, cooldown);
  }
};

function setAttributes(element, list) {
  try {
      Object.keys(list).forEach(key => element.setAttribute(key, list[key]));
  } catch (error) {
    console.warn('Error on applying attibutes list', list, 'to an element', element);
    console.warn(error);
  }

  return element;
}

const generateId = (() => {
  let counter = 0;

  return () => counter++;
})();

const isObject = (obj) => typeof(obj) === 'object';
const isNotObject = (obj) => !isObject(obj);

const isString = (str) => {
  return typeof(str) === 'string'
    ? { empty: str.length === 0, notEmpty: str.length > 0 }
    : false;
};

const isFunction = (obj) => typeof(obj) === 'function';
const isNotFunction = (obj) => !isFunction(obj);

const isNotArray = (obj) => !Array.isArray(obj);
const isSameArray = function (a1, a2) {
  if (isNotArray(a1) || isNotArray(a2))
    return false;

  if (a1.length !== a2.length)
    return false;

  if (a1.length === 0)
    return true;

  const aa1 = a1.sort();
  const aa2 = a2.sort();

  for (let i = 0; i < aa1.length; i++)
    if (JSON.stringify(aa1[i]) !== JSON.stringify(aa2[i]))
      return false;

  return true;
};
const isNotSameArray = (a1, a2) => !isSameArray(a1, a2);

module.exports = window.HELPERS = Object.freeze({
  generateId,

  fetch,
  throttle,
  debounce,
  setAttr: setAttributes,

  isObject,
  isNotObject,

  isString,

  isFunction,
  isNotFunction,

  isNotArray,
  isSameArray,
  isNotSameArray,
});

},{}],37:[function(require,module,exports){
// require main global styles
require('../css/main.scss');

// extend some JS objects
require('./helpers/extensions');

// add extra helpers to window object
require('./helpers/helpers');

// define app's constants
require('./helpers/constants');

const AppController = require('./App.Controller');
const DataChart = require('./primitive/DataChart');

class App {
  constructor() {
    this.main = new AppController(document.body);
    this.main.theme = CONSTANTS.THEMES.NIGHT;

    window.app = this;
  }

  async start() {
    this.main.model.isProcess = true;

    try {
      const {target: {response = ''}} = await HELPERS.fetch(CONSTANTS.DATA_URI);

      const charts = JSON.parse(response);

      if (HELPERS.isNotArray(charts))
        throw `Input data recieved at ${CONSTANTS.DATA_URI} contains invalid data!`;

      /*
      ** Transform raw Telegram's charts to consumable representation
      */
      const consumable = charts.map((c, i) => new DataChart(`Chart #${i}`, c));

      this.main.charts = consumable;
      this.main.selectionFrame = null;
      this.main.selectedChartsIndexes = consumable.length ? [0] : [];
    } catch (error) {
      console.error('Failed to load application. See below for details.');
      console.error(error);
    }

    this.main.model.isProcess = false;

    return this;
  }
};

new App().start();

},{"../css/main.scss":3,"./App.Controller":5,"./helpers/constants":34,"./helpers/extensions":35,"./helpers/helpers":36,"./primitive/DataChart":41}],38:[function(require,module,exports){
const Events = require('./Events');

module.exports = class BaseController extends Events {
  constructor(name, parent, model, view) {
    super();

    this.name = name;
    this.model = new model();
    this.view = new view(name, parent, this.model, this);
  }
};

},{"./Events":45}],39:[function(require,module,exports){
const {isObject, isNotArray, isFunction, isNotFunction} = require('../helpers/helpers');

module.exports = class BaseModel {
  constructor(props) {
    if (isObject(props) && isNotArray(props) && isNotFunction(props))
      for (let prop in props)
        createObservableProperty.call(this, prop, props[prop]);
  }

  batch(props) {
    if (isObject(props) && isNotArray(props) && isNotFunction(props))
      for (let prop in props)
        if (this.hasOwnProperty(prop))
          this[prop] = props[prop];
  }
};

function createObservableProperty(prop, value) {
  const _listeners = [];

  // internal keeper of model's property data
  const _name = `_${prop}`;
  //
  Object.defineProperty(this, _name, {
    value: value,
    writable: true,
    enumerable: false,
    configurable: false,
  });
  // // //

  // add listener callback
  const _fName = `on${prop.capitalize()}Changed`;
  //
  Object.defineProperty(this, _fName, {
    value: (callback) => isFunction(callback) && _listeners.push(callback),
    writable: false,
    enumerable: false,
    configurable: false,
  });
  // // //

  // define object's public enumerable property
  Object.defineProperty(this, prop, {
    get: () => { return this[_name]; },
    set: (v) => {
      const prev = this[_name];

      this[_name] = v;

      _listeners.forEach((callback) => {
        try {
          callback(v, prev);
        } catch(err) {
          console.warn(`Thrown error on observable property ${_name} callback`);
          console.warn(callback);
          console.warn(err);
        }
      });
    },
    enumerable: true,
  });
  // // //
};

},{"../helpers/helpers":36}],40:[function(require,module,exports){
module.exports = class BaseView {
  constructor(name, parent, model, controller) {
    this.el = null;
    this.name = name;
    this.parent = parent;
    this.model = model;
    this.controller = controller;

    this.build(parent);

    if (HELPERS.isNotObject(this.el))
      throw `ControlView ${name} has no 'el' object set`;

    this.exposeElements(controller);
    this.bindController(controller);
    this.bindToModel(model);
    this.setDefaults();
  }

  build() {
    throw `ControlView: [${this.name}] does not implement 'build' method`;
  }

  exposeElements() {
    console.warn(`ControlView [${this.name}] does not implement 'exposeElements' method`);
  }

  bindController() {
    console.info(`ControlView [${this.name}] does not implement 'bindController' method`);
  }

  bindToModel() {
    console.info(`ControlView [${this.name}] does not implement 'bindToModel' method`);
  }

  setDefaults() {
  }
};

},{}],41:[function(require,module,exports){
/*
**
** Converts Telegram's chart struct to consumable chart data
**
*/
const DataChartLine = require('./DataChartLine');

module.exports = class Chart {
  //raw data in Telegram's format
  constructor(name, {colors, columns, types, names}) {
    this.uid = HELPERS.generateId();
    this.name = name;

    const parts = columns.map(column => {
      const id = column[0];

      return new DataChartLine({
        id,
        type: types[id],
        name: names[id],
        color: colors[id],
        data: column.slice(1),
      });
    });

    this.lines = parts.filter(part => part.isLine);
    this.axis = parts.find(part => part.isXAxis);
  }

  /*
  ** Returns bounds of a chart
  */
  get bounds() {
    if (this.lines.filter(l => l.isEnabled).length === 0)
      return null;
    else
      return {
        x_max: Math.max(...this.axis.data),
        x_min: Math.min(...this.axis.data),
        y_max: Math.max(...this.lines.filter(l => l.isEnabled).map(l => l.bounds.max)),
        y_min: Math.min(...this.lines.filter(l => l.isEnabled).map(l => l.bounds.min)),
      };
  }
};

},{"./DataChartLine":42}],42:[function(require,module,exports){
/*
**
** Contains info about chart's line
**
*/
module.exports = class DataChartLine {
  constructor({id, type, name, color, data}) {
    this.uid = HELPERS.generateId();

    this.id = id;
    this.type = type;
    this.name = name;
    this.color = color;
    this.data = data.slice();
    this.isEnabled = true;

    this.bounds = {
      min: Math.min(...data),
      max: Math.max(...data),
    };
  }

  get isLine() {
    return this.type === CONSTANTS.PART_TYPE.LINE;
  }

  get isXAxis() {
    return this.type === CONSTANTS.PART_TYPE.X_AXIS;
  }
}

},{}],43:[function(require,module,exports){
module.exports = (superclass) => class extends superclass {
  /*
  ** sort charts by max X value
  */
  sortCharts(c1, c2) {
    return (c1.bounds === null || c2.bounds === null)
      ? 0
      : c1.bounds.x_min - c2.bounds.x_min;
  }

  /*
  ** Extract meta information about charts - colors, names, etc...
  */
  extractMeta(charts) {
    charts.forEach(chart => {
      chart.lines.forEach(({uid, id, color, name, isEnabled}) => {
        this.meta[uid] = ({uid, id, color, name, isEnabled});
      });
    });
  }

  /*
  ** Extract info about chart's bounds data - min/max values
  */
  extractBounds(charts) {
    this.bounds = {
      x_max: Number.MIN_SAFE_INTEGER,
      x_min: Number.MAX_SAFE_INTEGER,
      y_max: Number.MIN_SAFE_INTEGER,
      y_min: Number.MAX_SAFE_INTEGER,
      x_delta: 0,
      y_delta: 0,
    };

    for (let i = 0; i < charts.length; i++) {
      const bounds = charts[i].bounds;

      if (bounds === null)
        continue;

      this.bounds.x_max = Math.max(this.bounds.x_max, bounds.x_max);
      this.bounds.x_min = Math.min(this.bounds.x_min, bounds.x_min);
      this.bounds.y_max = Math.max(this.bounds.y_max, bounds.y_max);
      this.bounds.y_min = Math.min(this.bounds.y_min, bounds.y_min);
    }

    this.bounds.x_delta = this.bounds.x_max - this.bounds.x_min;
    this.bounds.y_delta = this.bounds.y_max - this.bounds.y_min;
  }

  /*
  ** Compile all chart's data into single data-object
  ** It ocntains x axis values and y array of objects
  ** with corresponding line uid's values
  */
  compileDataObject(chart) {
    for(let tIndex = 0; tIndex < chart.axis.data.length; tIndex++) {
      this.data.x.push(chart.axis.data[tIndex]);
      this.data.y[this.data.x.length - 1] = chart.lines.reduce((acc, {uid, data}) => {
        acc[uid] = data[tIndex];
        return acc;
      }, {});
    }
  }

  /*
  ** Returns only select data range
  */
  extractSelectionFrame(selectionFrame) {
    const offsetX = Math.floor(selectionFrame.left * this.data.x.length);
    const widthOffset = Math.ceil((selectionFrame.right - selectionFrame.left) * this.data.x.length);
    const data = {x: [], y: []};
    const bounds = {
      x_max: Number.MIN_SAFE_INTEGER,
      x_min: Number.MAX_SAFE_INTEGER,
      y_max: Number.MIN_SAFE_INTEGER,
      y_min: Number.MAX_SAFE_INTEGER,
    }

    let cnt = 0;
    while(cnt < widthOffset) {
      const x = this.data.x[offsetX + cnt];
      const y = this.data.y[offsetX + cnt];

      const y_vals = Object.keys(y).map(key => y[key]);

      bounds.x_max = Math.max(bounds.x_max, x);
      bounds.x_min = Math.min(bounds.x_min, x);
      bounds.y_min = Math.min(bounds.y_min, ...y_vals);
      bounds.y_max = Math.max(bounds.y_max, ...y_vals);

      data.x.push(x);
      data.y.push(y);

      cnt++;
    }

    const timelineLength = (bounds.x_max - bounds.x_min) / CONSTANTS.ONE_DAY_LENGTH;

    return {data, bounds, timelineLength};
  }
};

},{}],44:[function(require,module,exports){
/*
**
** Mega-dataset - concat all selected charts into single data object
**
*/
const Events = require('./Events');

const Ext = require('./DataGrid.ext');

module.exports = class DataGrid extends Ext(Events) {
  constructor(charts) {
    super();

    this.cache = charts.slice();
    this.data = {
      x: [],
      y: [],
    };
    this.bounds = {};
    this.meta = {};

    this.extractMeta(charts);
    this.extractBounds(charts);
    this.compile(charts);
  }

  /*
  ** Calculates size of a timeline
  */
  get timelineLength() {
    return this.bounds.x_delta / CONSTANTS.ONE_DAY_LENGTH;
  }

  /*
  ** We have something to show, don't we?
  */
  get noDataAvaliable() {
    return this.data === void 0
      || Object.keys(this.data).length === 0
      || Object.keys(this.meta).length === 0
      || this.cache.some(({lines}) => lines.some(line => line.isEnabled)) === false;
  }

  /*
  ** Compiles all charts into single data-object
  */
  compile(charts) {
    charts
      .sort(this.sortCharts) // we mus sort all in accordance with timeline first
      .forEach(this.compileDataObject.bind(this)); // then we can compile data
  }

  /*
  ** Toggle line???
  */
  toggleLine({uid, state}) {
    // toggle line in a cached data and meta!
    this.data = { x: [], y: [] };
    this.cache.forEach(({lines}) => lines.forEach(line => line.isEnabled = uid === line.uid ? state : line.isEnabled));
    this.meta[uid].isEnabled = state;

    // recalculate
    this.extractBounds(this.cache);
    this.compile(this.cache);

    // notify
    this.fire('lineVisibilityChanged', {uid, state});
  }
}

},{"./DataGrid.ext":43,"./Events":45}],45:[function(require,module,exports){
const {isNotFunction, isNotArray} = require('../helpers/helpers');

module.exports = class Events {
  constructor() {
    this._events = [];
    this._callbacks = new WeakMap();
  }

  _findEventKey(eventName) {
    return this._events.find(entry => entry.toString() === eventName);
  }

  on(eventName, callback) {
    if (isNotFunction(callback))
      return;

    if(this._findEventKey(eventName) === void 0)
      this._events.push(new String(eventName));

    const key = this._findEventKey(eventName);
    const listeners = this._callbacks.get(key);

    isNotArray(listeners)
      ? this._callbacks.set(key, [callback])
      : listeners.push(callback);
  }

  fire(eventName, data) {
    const key = this._findEventKey(eventName);

    if(key === void 0)
      return;

    const listeners = this._callbacks.get(key);

    Array.from(listeners).forEach((callback) => {
      try {
        callback(data);
      } catch (error) {
        console.warn(`Failed to execute callback on event ${eventName}`);
        console.warn(callback);
        console.warn(error);
      }
    })
  }
};

},{"../helpers/helpers":36}]},{},[37]);
